package com.hamedsafari.android_mubi_assessment.di

import com.hamedsafari.android_mubi_assessment.data.network.dataSource.FilmDataSource
import com.hamedsafari.android_mubi_assessment.data.network.dataSource.FilmNetworkDataSource
import com.hamedsafari.android_mubi_assessment.data.network.service.FilmService
import com.hamedsafari.android_mubi_assessment.data.network.util.ErrorHandler
import com.hamedsafari.android_mubi_assessment.data.network.util.ErrorHandlerImpl
import com.hamedsafari.android_mubi_assessment.data.repository.FilmRepository
import com.hamedsafari.android_mubi_assessment.data.repository.FilmRepositoryImpl
import com.hamedsafari.android_mubi_assessment.ui.FilmDetailViewModelFactory

/**
 * A Container for the Dependency Injection.
 * We can use Library's like Dagger, hilt, koin , ...
 * According to the scope of the project I prefer to do the manual way of DI
 * And don't add a library. ¯\_(ツ)_/¯
 */
class AppContainer {
    //Service
    private val service: FilmService by lazy { FilmService.create() }

    //Error Handler
    private val errorHandler: ErrorHandler by lazy { ErrorHandlerImpl() }

    //Datasource
    private val dataSource: FilmDataSource by lazy { FilmNetworkDataSource(service, errorHandler) }

    //Repository
    private val repository: FilmRepository by lazy { FilmRepositoryImpl(dataSource) }

    //Presentation - viewModelFactory
    val filmDetailViewModelFactory by lazy { FilmDetailViewModelFactory(repository) }

}