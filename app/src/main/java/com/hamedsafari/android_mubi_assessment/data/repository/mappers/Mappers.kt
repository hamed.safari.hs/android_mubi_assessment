package com.hamedsafari.android_mubi_assessment.data.repository.mappers

import com.hamedsafari.android_mubi_assessment.data.network.model.FilmApiModel
import com.hamedsafari.android_mubi_assessment.domain.Film

/**
 * Converts the network model to the domain model
 */
internal fun FilmApiModel.toDomainModel(): Film =
    Film(
        title = title.orEmpty(),
        coverUrl = still_url.orEmpty(),
        webUrl = web_url.orEmpty()
    )