package com.hamedsafari.android_mubi_assessment.data.repository

import com.hamedsafari.android_mubi_assessment.data.network.dataSource.FilmDataSource
import com.hamedsafari.android_mubi_assessment.utils.Result
import com.hamedsafari.android_mubi_assessment.domain.Film
import com.hamedsafari.android_mubi_assessment.data.repository.mappers.toDomainModel

/**
 * An interface to represent a repository that returns a Film.
 */
interface FilmRepository {
    /**
     * Returns Film.
     *
     * Assumptions:
     *
     *  - Film returned.
     */
    suspend fun fetchFilm(): Result<Film>
}

/**
 *  An implementation for the repository which fetches Film from dataSource.
 */
internal class FilmRepositoryImpl(
    private val dataSource: FilmDataSource
) : FilmRepository {

    override suspend fun fetchFilm(): Result<Film> =
        dataSource.fetchFilm().mapOnSuccess {
            it.toDomainModel()
        }
}
