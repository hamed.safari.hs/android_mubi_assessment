package com.hamedsafari.android_mubi_assessment.data.network.model

import com.google.gson.annotations.SerializedName

data class FilmApiModel(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("web_url")
    var web_url: String? = null,
    @SerializedName("still_url")
    var still_url: String? = null
)



