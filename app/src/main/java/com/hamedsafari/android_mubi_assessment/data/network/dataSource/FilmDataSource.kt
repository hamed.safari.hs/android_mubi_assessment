package com.hamedsafari.android_mubi_assessment.data.network.dataSource

import com.hamedsafari.android_mubi_assessment.data.network.model.FilmApiModel
import com.hamedsafari.android_mubi_assessment.data.network.service.FilmService
import com.hamedsafari.android_mubi_assessment.data.network.util.ErrorHandler
import com.hamedsafari.android_mubi_assessment.utils.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * An interface to represent a dataSource that returns a [FilmApiModel].
 *  * Note:  Relying on interfaces makes API implementations swappable.
 *      In addition to providing scalability and allowing to replace dependencies more easily,
 *      it also favors testability because we can inject fake data source implementations in tests.
 */
interface FilmDataSource {
    suspend fun fetchFilm(): Result<FilmApiModel>
}

/**
 *  An implementation for the FilmDataSource which returns [FilmApiModel] based on a Network Data.
 *
 *  * Note: this dataSource uses Retrofit Interface for networking, Although retrofit handles threading
 *      internally and main-safe but FilmNetworkDataSource handles thread it self with CoroutineDispatcher
 *      for the case of replacing retrofit or implement networking with HttpURLConnection.
 *      https://developer.android.com/reference/java/net/HttpURLConnection
 */
internal class FilmNetworkDataSource(
    private val service: FilmService,
    private val errorHandler: ErrorHandler,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : FilmDataSource {
    override suspend fun fetchFilm(): Result<FilmApiModel> {
        return try {
            withContext(dispatcher) { Result.Success(service.fetchFilm()) }
        } catch (e: Exception) {
            Result.Error(errorHandler.handle(e))
        }
    }
}