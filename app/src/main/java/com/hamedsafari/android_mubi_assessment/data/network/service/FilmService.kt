package com.hamedsafari.android_mubi_assessment.data.network.service

import com.hamedsafari.android_mubi_assessment.data.network.model.FilmApiModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header

/**
 * Used to connect to the mubi API to fetch film
 */
interface FilmService {

    //Todo add Interceptor for Headers
    @GET("v3/films/decision-to-leave")
    suspend fun fetchFilm(
        @Header("CLIENT") client: String = "android",
        @Header("CLIENT-VERSION") clientVersion: String = "14.0",
        @Header("CLIENT-APP") clientApp: String = "mubi",
        @Header("CLIENT-DEVICE-IDENTIFIER") clientDevice: String = "555",
        @Header("Content-Type") contentType: String = "application/json",
        @Header("Accept") accept: String = "application/json",
        @Header("CLIENT-COUNTRY") country: String = "GB"
    ): FilmApiModel

    companion object {
        private const val BASE_URL = "https://api.mubi.com/"

        fun create(): FilmService {
            val logger = HttpLoggingInterceptor().apply {
                level = Level.BASIC
                level = Level.BODY
            }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(FilmService::class.java)
        }
    }
}