package com.hamedsafari.android_mubi_assessment.domain

/**
 * A data class that holds details about a Film.
 *
 * @property title: String – Title of the Film.
 * @property coverUrl: String - Image Url of the Film.
 * @property webUrl: String - Web Url of the Film.
 *
 */
data class Film(
    val title: String,
    val coverUrl: String,
    val webUrl: String,
)
