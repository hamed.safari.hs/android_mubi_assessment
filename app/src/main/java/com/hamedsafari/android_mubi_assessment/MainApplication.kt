package com.hamedsafari.android_mubi_assessment

import android.app.Application
import com.hamedsafari.android_mubi_assessment.di.AppContainer

/**
 * Application class.
 */
class MainApplication : Application() {
    // Instance of AppContainer that will be used by all the Fragments of the app
    val appContainer = AppContainer()
}