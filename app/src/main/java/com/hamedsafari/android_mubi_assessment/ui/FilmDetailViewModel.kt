package com.hamedsafari.android_mubi_assessment.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.hamedsafari.android_mubi_assessment.data.repository.FilmRepository
import com.hamedsafari.android_mubi_assessment.domain.Film
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class FilmDetailViewModel(
    private val repository: FilmRepository
) : ViewModel() {

    private val _uiState = MutableStateFlow(FilmUiState())
    val uiState: StateFlow<FilmUiState> = _uiState.asStateFlow()

    init {
        getFilmDetail()
    }

    private fun getFilmDetail() = viewModelScope.launch {

        val result = repository.fetchFilm()

        result.mapOnSuccess { film ->
            _uiState.update { it.copy(filmDetail = film , isFetchingFilm = false) }
        }

    }

    data class FilmUiState(
        val isFetchingFilm: Boolean = true,
        val filmDetail: Film? = null
    )
}

@Suppress("UNCHECKED_CAST")
class FilmDetailViewModelFactory(private val repository: FilmRepository) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = FilmDetailViewModel(repository) as T
}