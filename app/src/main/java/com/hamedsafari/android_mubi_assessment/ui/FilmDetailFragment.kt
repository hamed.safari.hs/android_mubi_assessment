package com.hamedsafari.android_mubi_assessment.ui

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.hamedsafari.android_mubi_assessment.MainApplication
import com.hamedsafari.android_mubi_assessment.databinding.FragmentFilmDetailBinding
import com.hamedsafari.android_mubi_assessment.utils.ImageLoader
import com.hamedsafari.android_mubi_assessment.utils.collectLifecycleFlow

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FilmDetailFragment : Fragment() {

    private var _binding: FragmentFilmDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<FilmDetailViewModel> {
        val appContainer = (activity?.application as MainApplication).appContainer
        appContainer.filmDetailViewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFilmDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observerUIState()

    }

    private fun observerUIState() {
        collectLifecycleFlow(viewModel.uiState) {

            it.filmDetail?.let { film ->
                ImageLoader.loadImage(binding.imageCover, film.coverUrl)
                binding.textviewTitle.text = film.title
                binding.buttonShare.setOnClickListener { shareFilm(film.title, film.webUrl) }
            }

            it.isFetchingFilm.let { isFetching ->
                binding.buttonShare.isVisible = isFetching.not()
                binding.progressView.isVisible = isFetching
            }

        }
    }

    private fun shareFilm(title: String, url: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, title)
        startActivity(shareIntent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}