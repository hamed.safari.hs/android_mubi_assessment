package com.hamedsafari.android_mubi_assessment.ui

import com.hamedsafari.android_mubi_assessment.data.fake.FakeFilm
import com.hamedsafari.android_mubi_assessment.data.fake.MockFilmDataSource
import com.hamedsafari.android_mubi_assessment.data.fake.MockFilmRepository
import com.hamedsafari.android_mubi_assessment.data.repository.mappers.toDomainModel
import com.hamedsafari.android_mubi_assessment.rules.TestDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class FilmDetailViewModelTest {
    @get:Rule
    val testDispatcher = TestDispatcherRule()

    @Test
    fun filmDetailViewModel_getFilmDetail_verifyFilm() =
        runTest {
            //subject
            val viewModel = FilmDetailViewModel(
                repository = MockFilmRepository(MockFilmDataSource(FakeFilm.film))
            )

            //Mock data
            val mockData = MockFilmDataSource(FakeFilm.film).fetchFilm()
                .mapOnSuccess { it.toDomainModel() }
                .data()

            assertEquals(mockData, viewModel.uiState.value.filmDetail)
        }
}