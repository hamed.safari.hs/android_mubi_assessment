package com.hamedsafari.android_mubi_assessment.data.fake

import com.hamedsafari.android_mubi_assessment.data.network.model.FilmApiModel

object FakeFilm {
    val film = FilmApiModel(
        id = "1",
        title = "Decision to Leave",
        slug = "decision-to-leave",
        web_url = "https://mubi.com/films/decision-to-leave",
        still_url = "https://images.mubicdn.net/images/film/281688/cache-772890-1651137929/image-w1280.jpg"
    )
}