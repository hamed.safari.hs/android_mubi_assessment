package com.hamedsafari.android_mubi_assessment.data.fake

import com.hamedsafari.android_mubi_assessment.data.network.dataSource.FilmDataSource
import com.hamedsafari.android_mubi_assessment.data.repository.FilmRepository
import com.hamedsafari.android_mubi_assessment.data.repository.mappers.toDomainModel
import com.hamedsafari.android_mubi_assessment.domain.Film
import com.hamedsafari.android_mubi_assessment.utils.Result


class MockFilmRepository(
    private val dataSource: FilmDataSource
) : FilmRepository {
    override suspend fun fetchFilm(): Result<Film> {
        return dataSource.fetchFilm().mapOnSuccess { it.toDomainModel() }
    }
}