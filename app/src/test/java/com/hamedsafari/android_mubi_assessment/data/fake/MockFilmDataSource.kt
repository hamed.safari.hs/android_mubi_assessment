package com.hamedsafari.android_mubi_assessment.data.fake

import com.hamedsafari.android_mubi_assessment.data.network.dataSource.FilmDataSource
import com.hamedsafari.android_mubi_assessment.data.network.model.FilmApiModel
import com.hamedsafari.android_mubi_assessment.utils.Result

class MockFilmDataSource(private val film: FilmApiModel?) : FilmDataSource {
    override suspend fun fetchFilm(): Result<FilmApiModel> {
        film?.let { return Result.Success(it) }
        return Result.Error(
            Exception("Film not found")
        )
    }
}