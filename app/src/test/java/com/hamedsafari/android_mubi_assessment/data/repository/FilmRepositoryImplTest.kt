package com.hamedsafari.android_mubi_assessment.data.repository

import com.hamedsafari.android_mubi_assessment.data.fake.FakeFilm
import com.hamedsafari.android_mubi_assessment.data.fake.MockFilmDataSource
import com.hamedsafari.android_mubi_assessment.data.network.dataSource.FilmDataSource
import com.hamedsafari.android_mubi_assessment.data.repository.mappers.toDomainModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class FilmRepositoryImplTest() {
    private lateinit var dataSource: FilmDataSource
    private lateinit var repository: FilmRepository

    @Before
    fun setUp() {
        dataSource = MockFilmDataSource(FakeFilm.film)
        repository = FilmRepositoryImpl(dataSource)
    }

    @Test
    fun filmRepository_fetchFilm_verifyFilm() =
        runTest {
            val mockData = MockFilmDataSource(FakeFilm.film).fetchFilm()
                .mapOnSuccess { it.toDomainModel() }

            assertEquals(mockData, repository.fetchFilm())
        }
}