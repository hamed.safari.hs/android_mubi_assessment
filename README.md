# Task

## Description
The task is to create a rudimentary Android app implementing a very simple sharing screen:

* Get the film data from the backend. The relevant fields in the response are title, web_url and still_url. See next chapter for details.

* Implement a screen showing the film still (film.still_url) and a button.

* When the user presses the button, an Android Share Dialog should be shown (https://developer.android.com/training/sharing/send).

* The film title (film.title) and the film url (film.still_url) are passed to the Share Dialog.


## Design
<img width="269" alt="design" src="https://user-images.githubusercontent.com/148168/197551935-87c53982-0a98-4bb6-a3f3-ad4c73dcfa3c.png">


The MUBI logo is in this repositry (ic_mubi_logo.xml).

## API
### Get Film 

`GET: https://api.mubi.com/v3/films/decision-to-leave`

Response: 
```
{
	"id": 281688,
	"slug": "decision-to-leave",
	"title": "Decision to Leave",
	"web_url": "https://mubi.com/films/decision-to-leave",
	"still_url": "https://images.mubicdn.net/images/film/281688/cache-772890-1651137929/image-w1280.jpg",
	....
}
```
One way to work with REST interface in Android is to use Retrofit https://square.github.io/retrofit/


### HTTP Headers
In order to get a response from the server, these http headers have to be set:

```
CLIENT: "android"
CLIENT-VERSION: "14.0"
CLIENT-APP: "mubi"
CLIENT-DEVICE-IDENTIFIER: "555"
Content-Type: "application/json"
Accept: "application/json"
CLIENT-COUNTRY: "GB"
```

### cUrl
```

curl -X "GET" "https://api.mubi.com/v3/films/decision-to-leave" \
	-H 'CLIENT: android' \
	-H 'CLIENT-VERSION: 14.0' \
	-H 'CLIENT-APP: mubi' \
	-H 'CLIENT-DEVICE-IDENTIFIER: 222' \
	-H 'Accept-Language: en' \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-H 'CLIENT-COUNTRY: GB'
		     
```

